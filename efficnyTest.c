//This is to test the program 

#include <stdio.h>
#include <time.h>
#include <stdlib.h>

long imul(long, long);
void popRandVals(long**, long, long);

int main(){
        long row, col;

        printf("Enter in a 2-D array to test\n\tROWS:");
        scanf("%ld", &row);
        printf("\n\tCOLS:");
        scanf("%ld", &col);

        //Make space for array
        long **arr = (long **)malloc(row *  sizeof(long *));

        *arr = (long *)malloc(sizeof(long) * row * col);

        //Postion allocated memory across COL POINTERS
        for (long i=0; i<row; i++)
                arr[i]=(*arr +col*i);

        //long test1[TST1_ROW][TST1_COL];
        //long test2[100][1]

        popRandVals(arr, row, col);


        clock_t begin = clock();

        imul(2,3);

        clock_t end = clock();
        printf("Elapsed: %f second\n", (double)(end - begin) / CLOCKS_PER_SEC);
        free(*arr);
        free(arr);


        return 0;
}

long imul(long a, long b){
        long s = a * b;
        return s;
}

void popRandVals(long **arr, long row, long col){

        for (long j=0; j<row; j++){
                for (long k=0; k<col; k++){
                        *(*(arr+j)+k)=j+k;
                }
        }
}
