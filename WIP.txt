
.pos 0
irmovq stack, %rsp  	# Set up stack pointer
call main		# Execute main program
halt			# Terminate program 


.align 8

A:	
	.quad 0x0
	.quad 0x1
	.quad 0x2
	.quad 0x1
	.quad 0x2
	.quad 0x3

B:	
	.quad 0x0
	.quad 0x1
	.quad 0x1
	.quad 0x1
	.quad 0x2
	.quad 0x3

X:
	.quad 0x0
	.quad 0x0
	.quad 0x0
	.quad 0x0

main:
	irmovq A, %rdi
	irmovq B, %rsi
	irmovq X, %rdx
	irmovq 2, %rcx
	irmovq 3, %r8
	irmovq 2, %r9
	irmovq 0, %rax
	call mm
	
	irmovq X, %rdi
	irmovq $4, %rsi		# X[2][2] has 4 elements
	call sum
	ret

sum:	
	irmovq $8,%r8		# Constant 8
	irmovq $1,%r9		# Constant 1
	xorq %rax,%rax		# sum = 0
	andq %rsi,%rsi		# Set CC
	jmp	 stest		# Goto test
sloop:	
	mrmovq (%rdi),%r10	# Get *start
	addq %r10,%rax		# Add to sum
	addq %r8,%rdi		# start++
	subq %r9,%rsi		# count--.  Set CC
stest:
	jne	sloop		# Stop when 0
	ret			# Return

imul:
    # TODO: rax = rdi*rsi
    # Set up stack frame
    pushq %rbp
    rrmovq %rsp, %rbp
    pushq %r11
    
    irmovq $-1, %r11  # rsi = -1
    xorq %rax, %rax   # rax = 0

    # Skip multiply loop silently if multiplying by <= 0.
    andq %rsi, %rsi
    jle Multiply_End

Multiply_Loop:
    addq %rdi, %rax   # rax += rcx
    addq %r11, %rsi   # rdx -= 1
    jne Multiply_Loop # if (rdx != 0) goto Multiply_Loop

Multiply_End:
    # Clean up stack frame.
    popq %r11
    rrmovq %rbp, %rsp
    popq %rbp
    ret

mm:
	pushq %r12
	pushq %r13
	pushq %r14
	pushq %rbx
	pushq %rbp
	rrmovq %rsp, %rbp
	irmovq $-80, %rsp
	addq %rbp, %rsp
	
	rmmovq %rdi, -8(%rbp)
	rmmovq %rsi, -16(%rbp)
	rmmovq %rdx, -24(%rbp)
	rmmovq %rcx, -32(%rbp)
	rmmovq %r8, -40(%rbp)
	rmmovq %r9, -48(%rbp)

	irmovq $0, %r12		# i
iloop:
	mrmovq -40(%rbp), %rsi
	rrmovq %r12, %rdi
	call imul
	rmmovq %rax, -56(%rbp)	# i*ac

	mrmovq -48(%rbp), %rsi
	rrmovq %r12, %rdi
	call imul
	rmmovq %rax, -64(%rbp)	# i*bc

	irmovq $0, %r13 #j
jloop:
	mrmovq -24(%rbp), %rdx	# X
	mrmovq -64(%rbp), %rbx	# i*bc
	
	addq %r13, %rbx
	addq %rbx, %rbx
	addq %rbx, %rbx
	addq %rbx, %rbx		# (i*bc+j)*8
	addq %rdx, %rbx		# &X[i][j]

	irmovq $0, %r14		# k
kloop:
	mrmovq -48(%rbp), %rsi
	rrmovq %r14, %rdi
	call imul
	mrmovq -16(%rbp), %rsi
	addq %r13, %rax
	addq %rax, %rax
	addq %rax, %rax
	addq %rax, %rax		# (k*bc+j)*8
	addq %rax, %rsi	 	# &B[k][j]
	mrmovq 0(%rsi), %rsi
	
	mrmovq -56(%rbp), %rax  # i*ac
	mrmovq -8(%rbp), %rdi
	addq %r14, %rax
	addq %rax, %rax
	addq %rax, %rax
	addq %rax, %rax   	# (i*ac+k)*8
	addq %rax, %rdi  	# &A[i][k]
	mrmovq 0(%rdi), %rdi
	
	call imul
	
	mrmovq 0(%rbx), %rdi
	addq %rax,%rdi
	rmmovq %rdi, 0(%rbx)

ktest:
	irmovq $1, %rdi
	mrmovq -40(%rbp), %r8
	addq %rdi, %r14
	subq %r14, %r8
	jg kloop

jtest:
	irmovq $1, %rdi
	mrmovq -48(%rbp), %r9
	addq %rdi, %r13
	subq %r13, %r9
	jg jloop
	
itest:
	irmovq $1, %rdi
	mrmovq -32(%rbp), %rcx
	addq %rdi, %r12
	subq %r12, %rcx
	jg iloop

end:
	rrmovq %rbp, %rsp
	popq %rbp
	popq %rbx
	popq %r14
	popq %r13
	popq %r12
	ret


# Stack starts here and grows to lower addresses
	.pos 0x1000
stack:

# Stack starts here and grows to lower addresses
	.pos 0x1000
stack:
