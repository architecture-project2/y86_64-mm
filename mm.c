/**
 * Gedare Bloom
 *
 * Test code in C
 */

#include <stdlib.h>
#include <stdio.h>
#include<time.h>

#define DIM1 10
#define DIM2 3
#define DIM3 DIM2
#define DIM4 10

#define SCENARIO (1)

void mm(long **A, long **B, long **X, int ar, int ac, int bc)
{
  int i, j, k;

  for (i = 0; i < ar; i++)
    for (j = 0; j < bc; j++)
      for (k = 0; k < ac; k++){
         printf("\tX[%d][%d]+=A[%d][%d]*B[%d][%d]:", i, j, i, k, k, j);
         printf("%ld += %ld * %ld\n", X[i][j], A[i][k], B[k][j]);
         X[i][j] += A[i][k] * B[k][j];
       }
}


int main(int argc, char *argv[])
{
  long **A, **B, **X;
  int i,j;
  long long sum = 0;

  A = malloc(DIM1*sizeof(long*));
  for (i = 0; i < DIM1; i++)
    A[i] = malloc(DIM2*sizeof(long));

  B = malloc(DIM3*sizeof(long*));
  for (i = 0; i < DIM3; i++)
    B[i] = malloc(DIM4*sizeof(long));

  X = malloc(DIM1*sizeof(long*));
  for (i = 0; i < DIM4; i++)
    X[i] = malloc(DIM4*sizeof(long));

  clock_t begin = clock();
  printf("MATRIX A:\n");
  for (i = 0; i < DIM1; i++)
    for (j = 0; j < DIM2; j++){
#if SCENARIO == 1
      A[i][j] = i+j;
#else
      A[i][j] = -(i+j);
#endif
      printf("\n\t%ld", A[i][j]);
    }
  clock_t end = clock();
  //printf("%ld\n", CLOCKS_PER_SEC);
  printf("\n\nElapsed: %f second\n", (long)(end - begin)/CLOCKS_PER_SEC);

  printf("\n---------------------------------\n");

  printf("MATRIX B: \n");
  for (i = 0; i < DIM3; i++)
    for (j = 0; j < DIM4; j++){
      B[i][j] = i|j;
      printf("\n\t%ld", B[i][j]);

    }

  printf("\n---------------------------------\n");

  printf("MATRIX X:\n");
  for (i = 0; i < DIM1; i++)
    for (j = 0; j < DIM4; j++){
      X[i][j] = 0;
      printf("\n\t%ld", X[i][j]);
    }

  printf("\n---------------------------------\n");

  //clock_t begin = clock();
  mm(A, B, X, DIM1, DIM2, DIM4);
  //clock_t end = clock();
  //printf("Elapsed: %f second\n", (long)(end - begin) / CLOCKS_PER_SEC);

  printf("A x B=\n");
  for (i = 0; i < DIM1; i++)
    for (j = 0; j < DIM4; j++){
      sum += X[i][j];
      printf("\n\t%ld", X[i][j]);
    }

printf("\n---------------------------------\n");


  for (i = 0; i < DIM1; i++) free(A[i]);
  for (i = 0; i < DIM3; i++) free(B[i]);
  for (i = 0; i < DIM1; i++) free(X[i]);
  free(A); free(B); free(X);

  printf("The matrix sum is: %lld\n", sum);

  return 0;
}


